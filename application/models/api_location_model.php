<?php

class Api_location_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    private $m_loc 	= "m_loc";

    function isValidLocation($location){
    	$this->db->select("loc_id");
    	$this->db->from("m_loc");
    	$this->db->where("loc_name", $location);
    	return $this->db->get();
    }    

}