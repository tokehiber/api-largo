<?php

class Api_shipping_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    private $hr_user    = "hr_user";
    private $barang     = "barang";
    private $shipping   = "shipping";

    function getId($id, $wh, $kd, $tbl){
        $this->db->select($id);
        $this->db->from($tbl);
        $this->db->where($wh, $kd);
        return $this->db->get();
    }

    function isShipped($shippingCode){
    	$this->db->select("st_shipping");
    	$this->db->from("shipping");
    	$this->db->where("shipping_code", $shippingCode);
    	return $this->db->get();
    }

    function getItemCode($shippingCode){
    	$this->db->select("shipping_code, kd_barang, kd_unik, loc_name");
        $this->db->from("receiving_barang rcv");
        $this->db->join("barang brg", "rcv.id_barang = brg.id_barang", "left");
        $this->db->join("m_loc loc", "rcv.loc_id = loc.loc_id", "left");
        $this->db->join("shipping shp", "rcv.shipping_id = shp.shipping_id", "left");
        $this->db->where("shp.shipping_code", $shippingCode);
        $this->db->order_by("kd_barang");
        return $this->db->get();
    }

    function lockShipping($data){
        $this->db->set("shipping_date", $data["shipping_date"]);
        $this->db->set("st_shipping", 1);
        $this->db->where("shipping_code", $data["shipping_code"]);
        $this->db->update("shipping");
    }

    function setShipping($data){
        $id_shipper     = $this->getId("user_id", "user_name", $data["uname_ship"], $this->hr_user)->row_array();
        $id_barang      = $this->getId("id_barang", "kd_barang", $data["kd_barang"], $this->barang)->row_array();
        $id_shipping    = $this->getId("shipping_id", "shipping_code", $data["shipping_code"], $this->shipping)->row_array();
        $this->db->set("st_shipping", 1);
        $this->db->set("user_id_shipping", $id_shipper["user_id"]);
        $this->db->where("id_barang", $id_barang["id_barang"]);
        $this->db->where("kd_unik", $data["kd_unik"]);
        $this->db->where("shipping_id", $id_shipping["shipping_id"]);
        $this->db->update("receiving_barang");
    }
}