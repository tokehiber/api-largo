<?php

class Api_transfer_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    private $int_transfer 			= "int_transfer";
    private $int_transfer_detail 	= "int_transfer_detail";
    private $m_loc 					= "m_loc";
    private $receiving_barang		= "receiving_barang";
    private $hr_user				= "hr_user";

    function isTransfered($transfer_code){
    	$this->db->select("transfer_status");
    	$this->db->from($this->int_transfer);
    	$this->db->where("transfer_code", $transfer_code);
    	return $this->db->get();
    }

    function lockTransfer($transfer_code){
    	$this->db->set("transfer_status", 1);
    	$this->db->set("transfer_date", date("Y-m-d"));
    	$this->db->where("transfer_code", $transfer_code);
    	$this->db->update($this->int_transfer);
    }

    function getTransfer($data){
    	switch ($data['state']) {
    		case 'pick':
    			$this->db->select("a.loc_id, b.loc_name");
		    	$this->db->from("receiving_barang a");
		    	$this->db->join("m_loc b", "a.loc_id = b.loc_id", "left");
		    	$this->db->where("kd_unik", $data["kd_unik"]);
		    	return $this->db->get();
    			break;
    		case 'put':
    			$this->db->select("*");
		    	$this->db->from("int_transfer_detail a");
		    	$this->db->join("int_transfer b", "a.transfer_id = b.transfer_id", "left");
		    	$this->db->join("m_loc c", "a.loc_id_old = c.loc_id", "left");
		    	$this->db->join("hr_user d", "a.user_id_pick = d.user_id", "left");
		    	$this->db->where("a.kd_unik", $data["kd_unik"]);
		    	$this->db->where("b.transfer_code", $data["transfer_code"]);
		    	$this->db->where("c.loc_name", $data["loc_name_old"]);
		    	$this->db->where("d.user_name", $data["uname_pick"]);
		    	return $this->db->get();
    			break;
    		default:
    			# nothing to do.
    			break;
    	}
    }

    function getId($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
    	return $this->db->get();
    }

    function setTransfer($data){
    	switch ($data['state']) {
    		case 'pick':
    			$this->db->set("loc_id", 102);
    			$this->db->where("kd_unik", $data["kd_unik"]);
    			$this->db->update($this->receiving_barang);
    			$id_transfer = $this->getId("transfer_id", "transfer_code", $data["transfer_code"], $this->int_transfer)->row_array();
    			$id_picker = $this->getId("user_id", "user_name", $data["uname_pick"], $this->hr_user)->row_array();
    			$this->db->set("transfer_id", $id_transfer["transfer_id"]);
    			$this->db->set("kd_unik", $data["kd_unik"]);
    			$this->db->set("loc_id_old", $data["loc_id_old"]);
    			$this->db->set("user_id_pick", $id_picker["user_id"]);
    			$this->db->set("pick_time", date("Y-m-d H:i:s"));
    			$this->db->insert($this->int_transfer_detail);
    			break;
    		case 'put':
    			$id_loc_new = $this->getId("loc_id", "loc_name", $data["loc_name_new"], $this->m_loc)->row_array();
    			$id_loc_old = $this->getId("loc_id", "loc_name", $data["loc_name_old"], $this->m_loc)->row_array();
    			$this->db->set("loc_id", $id_loc_new["loc_id"]);
    			$this->db->where("kd_unik", $data["kd_unik"]);
    			$this->db->update($this->receiving_barang);
    			$id_transfer = $this->getId("transfer_id", "transfer_code", $data["transfer_code"], $this->int_transfer)->row_array();
    			$id_picker = $this->getId("user_id", "user_name", $data["uname_pick"], $this->hr_user)->row_array();
    			$id_putter = $this->getId("user_id", "user_name", $data["uname_put"], $this->hr_user)->row_array();
    			$this->db->set("loc_id_new", $id_loc_new["loc_id"]);
    			$this->db->set("user_id_put", $id_putter["user_id"]);
    			$this->db->set("put_time", $data["put_time"]);
    			$this->db->where("transfer_id", $id_transfer["transfer_id"]);
    			$this->db->where("loc_id_old", $id_loc_old["loc_id"]);
    			$this->db->where("user_id_pick", $id_picker["user_id"]);
    			$this->db->where("kd_unik", $data["kd_unik"]);
    			$this->db->update($this->int_transfer_detail);
    			break;
    		default:
    			# nothing to do.
    			break;
    	}
    }

    function updateByDelete($data){
    	$id_loc = $this->getId("loc_id", "loc_name", $data["loc_name"], $this->m_loc)->row_array();
    	$this->db->set("loc_id", $id_loc["loc_id"]);
    	$this->db->where("kd_unik", $data["kd_unik"]);
    	$this->db->update($this->receiving_barang);

    	$id_transfer = $this->getId("transfer_id", "transfer_code", $data["transfer_code"], $this->int_transfer)->row_array();
    	$this->db->where("transfer_id", $id_transfer["transfer_id"]);
    	$this->db->where("kd_unik", $data["kd_unik"]);
    	$this->db->where("loc_id_old", $id_loc["loc_id"]);
    	$this->db->delete($this->int_transfer_detail);
    }

}