<?php

class Api_serial_number_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function isValid($serial_number){
    	$this->db->select("*");
    	$this->db->from("receiving_barang");
    	$this->db->where("kd_unik", $serial_number);
    	return $this->db->get();
    }

    function isValidPicking($item_code, $serial_number){
    	$this->db->select("*");
    	$this->db->from("receiving_barang rcv");
    	$this->db->join("barang brg", "brg.id_barang = rcv.id_barang", "left");
    	$this->db->where("kd_unik", $serial_number);
    	$this->db->where("kd_barang", $item_code);
    	return $this->db->get();
    }

    function activeStock($serial_number){
        $this->db->select("kd_barang, nama_barang, loc_name, tgl_in, tgl_exp");
        $this->db->from("receiving_barang rcv");
        $this->db->join("barang brg", "rcv.id_barang = brg.id_barang", "left");
        $this->db->join("m_loc loc", "rcv.loc_id = loc.loc_id", "left");
        $this->db->where("kd_unik", $serial_number);
        $this->db->where("rcv.id_barang IS NOT NULL");
        $this->db->where("pl_status", 0);
        return $this->db->get();
    }

    function isValidActiveDetail($item_code){
        $this->db->select("*");
        $this->db->from("barang");
        $this->db->where("kd_barang", $item_code);
        return $this->db->get();
    }

    function countActiveStock($item_code){
        $this->db->select("COUNT(*) AS qty");
        $this->db->from("receiving_barang rcv");
        $this->db->join("barang brg", "rcv.id_barang = brg.id_barang", "left");
        $this->db->where("kd_barang", $item_code);
        return $this->db->get();
    }

    function getActiveDetail($item_code){
        $this->db->select("loc_name, count(kd_unik) AS qty");
        $this->db->from("receiving_barang rcv");
        $this->db->join("m_loc loc", "rcv.loc_id = loc.loc_id", "left");
        $this->db->join("barang brg", "rcv.id_barang = brg.id_barang", "left");
        $this->db->where("kd_barang", $item_code);
        $this->db->where("pl_status", 0);
        $this->db->where("loc.loc_id IS NOT NULL");
        $this->db->group_by("loc.loc_id");
        return $this->db->get();
    }
}