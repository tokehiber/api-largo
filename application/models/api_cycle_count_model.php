<?php

class Api_cycle_count_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    private $hr_user        = "hr_user";
    private $cycle_count    = "cycle_count";

    function getId($id, $wh, $kd, $tbl){
        $this->db->select($id);
        $this->db->from($tbl);
        $this->db->where($wh, $kd);
        return $this->db->get();
    }

    function isCycled($cc_code){
    	$this->db->select("status");
    	$this->db->from("cycle_count");
    	$this->db->where("cc_code", $cc_code);
    	return $this->db->get();
    }

    function getItemCode($cc_code){
    	$this->db->select("kd_barang, a.kd_unik, loc_name");
        $this->db->from("cycle_count_detail a");
        $this->db->join("receiving_barang rcv", "a.kd_unik = rcv.kd_unik", "left");
        $this->db->join("barang brg", "rcv.id_barang = brg.id_barang", "left");
        $this->db->join("m_loc loc", "rcv.loc_id = loc.loc_id", "left");
        $this->db->join("cycle_count cc", "a.cc_id = cc.cc_id", "left");
        $this->db->where("cc_code", $cc_code);
        $this->db->order_by("kd_barang");
        return $this->db->get();
    }

    function setCC($data){
        $id_user    = $this->getId("user_id", "user_name", $data["uname_cc"], $this->hr_user)->row_array();
        $id_cc      = $this->getId("cc_id", "cc_code", $data["cc_code"], $this->cycle_count)->row_array();
        $this->db->set("cc_status", 1);
        $this->db->set("user_id_cc", $id_user["user_id"]);
        $this->db->where("kd_unik", $data["kd_unik"]);
        $this->db->where("cc_id", $id_cc["cc_id"]);
        $this->db->update("cycle_count_detail");
    }

    function lockCC($data){
        $this->db->set("cc_time", $data["cc_time"]);
        $this->db->set("remark", $data["remark"]);
        $this->db->set("status", 1);
        $this->db->where("cc_code", $data["cc_code"]);
        $this->db->update("cycle_count");
    }

}