<?php

class Api_receiving_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    private $receiving 			= "receiving";
    private $barang 			= "barang";
    private $receiving_barang 	= "receiving_barang";
    private $hr_user			= "hr_user";

    function isReceived($receivingCode){
    	$this->db->select("st_receiving");
    	$this->db->from("receiving");
    	$this->db->where("kd_receiving", $receivingCode);
    	return $this->db->get();
    }

    function getItemCode($receivingCode){
    	$this->db->select("b.kd_barang, b.st_batch, b.has_expdate, a.qty");
    	$this->db->from("receiving_qty a");
    	$this->db->join("barang b", "b.id_barang = a.id_barang", "left");
    	$this->db->join("receiving c", "c.id_receiving = a.id_receiving", "left");
    	$this->db->where("c.kd_receiving", $receivingCode);
    	return $this->db->get();
    }

    function getId($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
    	return $this->db->get();
    }

    function lockReceiving($data){
        $this->db->set("tanggal_receiving", $data["tanggal_receiving"]);
        $this->db->set("st_receiving", 1);
        $this->db->set("remark", $data["remark"]);
        $this->db->where("kd_receiving", $data["kd_receiving"]);
        $this->db->update($this->receiving);
    }

    function setReceiving($data){
    	$id_barang 		= $this->getId("id_barang", "kd_barang", $data["kd_barang"], $this->barang)->row_array();
    	$id_receiving 	= $this->getId("id_receiving", "kd_receiving", $data["kd_receiving"], $this->receiving)->row_array();
    	$id_user 	 	= $this->getId("user_id", "user_name", $data["user_name"], $this->hr_user)->row_array();
        $this->db->set("id_barang", $id_barang["id_barang"]);
        $this->db->set("id_receiving", $id_receiving["id_receiving"]);
        $this->db->set("kd_batch", $data["kd_batch"]);
    	$this->db->set("loc_id", 100);
    	$this->db->set("tgl_exp", $data["tgl_exp"]);
    	$this->db->set("tgl_in", $data["tgl_in"]);
    	$this->db->set("st_receive", 1);
    	$this->db->set("user_id_receiving", $id_user["user_id"]);
    	$this->db->where("kd_unik", $data['kd_unik']);
    	$this->db->update($this->receiving_barang);
    }
}