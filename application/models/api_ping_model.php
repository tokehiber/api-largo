<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_ping_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getServer($app) {
    	return $this->db->get_where("hr_apps", array('app_name' => $app));
    }
}

/* End of file api_ping_model.php */
/* Location: ./application/models/api_ping_model.php */