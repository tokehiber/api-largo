<?php

class Api_user_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getUser($uname, $upass) {
    	return $this->db->get_where("hr_user", array("user_name" => $uname, "user_password" => $upass));
    }

    function updateUserSessionCode($uid, $newSessionCode) {
    	$data["handheld_session_code"] = $newSessionCode;
    	$this->db->where("user_id", $uid);
    	$this->db->update("hr_user", $data);
    }

    function getUserValidation($uname, $sessionCode) {
    	return $this->db->get_where("hr_user", array("user_name" => $uname, "handheld_session_code" => $sessionCode));
    }

    function isAuthorized(){
        $uname      = $this->input->get_request_header('USER', true);
        $session    = $this->input->get_request_header('Authorization', true);
        $valid      = $this->getUserValidation($uname, $session)->row_array();
        if($valid){
            return true;
        } else{
            return false;
        }
    }
}