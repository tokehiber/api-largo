<?php

class Api_settings_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getLocationList() {
    	$this->db->select("loc_name, loc_desc");
    	$this->db->from("m_loc");
    	return $this->db->get();
    }
}