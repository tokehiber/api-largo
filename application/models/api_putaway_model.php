<?php

class Api_putaway_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    private $m_loc 				= "m_loc";
    private $receiving_barang 	= "receiving_barang";
    private $hr_user			= "hr_user";

    function getPutawayList($loc){
    	$this->db->select("kd_barang, kd_unik, loc_name");
    	$this->db->from("receiving_barang a");
        $this->db->join("barang brg", "brg.id_barang = a.id_barang", "left");
    	$this->db->join("m_loc b", "b.loc_id = a.loc_id", "left");
    	$this->db->where("loc_name", $loc);
        $this->db->where("a.id_barang IS NOT NULL");
    	return $this->db->get();
    }

    function isAvailableSN($serial_number){
    	$this->db->select("loc_id");
    	$this->db->from("receiving_barang");
    	$this->db->where("kd_unik", $serial_number);
    	return $this->db->get();
    }

    function getId($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
    	return $this->db->get();
    }

    function setPutaway($data){
    	$id_loc 	= $this->getId("loc_id", "loc_name", $data["loc_name"], $this->m_loc)->row_array();
    	$id_user 	= $this->getId("user_id", "user_name", $data["user_name"], $this->hr_user)->row_array();
    	$this->db->set("loc_id", $id_loc["loc_id"]);
    	$this->db->set("user_id_putaway", $id_user["user_id"]);
    	$this->db->where("kd_unik", $data["kd_unik"]);
    	$this->db->update($this->receiving_barang);
    }

}