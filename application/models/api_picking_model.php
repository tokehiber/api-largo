<?php

class Api_picking_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    private $picking_list 	= "picking_list";
    private $barang 		= "barang";
    private $hr_user 		= "hr_user";

    function getId($id, $wh, $kd, $tbl){
    	$this->db->select($id);
    	$this->db->from($tbl);
    	$this->db->where($wh, $kd);
    	return $this->db->get();
    }

    function isPicked($pickingCode){
    	$this->db->select("pl_status");
    	$this->db->from("picking_list");
    	$this->db->where("pl_name", $pickingCode);
    	return $this->db->get();
    }

    function getItemCode($pickingCode){
    	$this->db->select("pl_name, kd_barang, qty");
    	$this->db->from("picking_qty a");
    	$this->db->join("picking_list b", "a.pl_id = b.pl_id", "left");
    	$this->db->join("barang brg", "a.id_barang = brg.id_barang", "left");
    	$this->db->where("b.pl_name", $pickingCode);
    	return $this->db->get();
    }

    function getLocationList($pickingCode){
    	$id_picking = $this->getId("pl_id", "pl_name", $pickingCode, $this->picking_list)->row_array();
    	$this->db->select("pl_name, kd_barang, loc_name, COUNT(*) AS qty");
    	$this->db->from("picking_qty a");
    	$this->db->join("picking_list p_list", "a.pl_id = p_list.pl_id", "left");
    	$this->db->join("barang brg", "a.id_barang = brg.id_barang", "left");
    	$this->db->join("receiving_barang rcv", "a.id_barang = rcv.id_barang", "left");
    	$this->db->join("m_loc loc", "rcv.loc_id = loc.loc_id", "left");
    	$this->db->where("a.pl_id", $id_picking["pl_id"]);
    	$this->db->where("loc_name IS NOT NULL");
    	$this->db->group_by("kd_barang");
    	$this->db->group_by("loc_name");
    	return $this->db->get();
    }

    function getBatch($batchCode){
    	$this->db->select("kd_batch, kd_barang, kd_unik, loc_name");
    	$this->db->from("receiving_barang rcv");
    	$this->db->join("barang brg", "rcv.id_barang = brg.id_barang", "left");
    	$this->db->join("m_loc loc", "rcv.loc_id = loc.loc_id", "left");
    	$this->db->where("kd_batch", $batchCode);
    	$this->db->where("pl_status", 0);
    	$this->db->where("loc_name IS NOT NULL");
    	$this->db->order_by("loc_name");
    	$this->db->order_by("kd_unik");
    	return $this->db->get();
    }

    function setPicking($data){
    	$id_barang 	= $this->getId("id_barang", "kd_barang", $data["kd_barang"], $this->barang)->row_array();
    	$id_picker 	= $this->getId("user_id", "user_name", $data["uname_pick"], $this->hr_user)->row_array();
    	if ($data["batch_code"] != "") {
            $id_batch = $this->getId("pl_id", "pl_name", $data["batch_code"], $this->picking_list)->row_array();
            $this->db->set("loc_id", 101);
            $this->db->set("pl_id", $id_batch["pl_id"]);
	    	$this->db->set("pl_status", 1);
	    	$this->db->set("user_id_picking", $id_picker["user_id"]);
	    	$this->db->where("id_barang", $id_barang["id_barang"]);
	    	$this->db->where("kd_unik", $data["kd_unik"]);
	    	$this->db->update("receiving_barang");
    	} else{
    		$id_picking	= $this->getId("pl_id", "pl_name", $data["picking_code"], $this->picking_list)->row_array();
            $this->db->set("loc_id", 101);
    		$this->db->set("pl_id", $id_picking["pl_id"]);
	    	$this->db->set("pl_status", 1);
	    	$this->db->set("user_id_picking", $id_picker["user_id"]);
	    	$this->db->where("id_barang", $id_barang["id_barang"]);
	    	$this->db->where("kd_unik", $data["kd_unik"]);
	    	$this->db->update("receiving_barang");
    	}
    }

    function lockPicking($data){
    	$this->db->set("pl_date", $data["picking_time"]);
    	$this->db->set("pl_status", 1);
    	$this->db->set("remark", $data["remark"]);
    	$this->db->where("pl_name", $data["picking_code"]);
    	$this->db->update("picking_list");
    }
}