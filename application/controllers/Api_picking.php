<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_picking extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('api_picking_model');
		$this->load->model('api_user_model');
	}

	public function index()
	{
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;
		echo json_encode($data);
	}

	public function getPicking($picking_code = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $picking_code == ''){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$pickedBefore	= $this->api_picking_model->isPicked($picking_code)->row_array();
				if($pickedBefore){
					if($pickedBefore['pl_status'] == 1){
						$data['status']		= 401;
						$data['param']		= $picking_code;
						$data['message']	= $picking_code . ' has been picked before.';
						$data['response']	= false;
					} else{
						$data['status']		= 200;
						$data['param']		= $picking_code;
						$data['message']	= $picking_code . ' is available.';
						$data['response']	= true;
						$data['results'] 	= $this->api_picking_model->getItemCode($picking_code)->result_array();
						$data['loc_list'] 	= $this->api_picking_model->getLocationList($picking_code)->result_array();
					}
				} else{
					$data['status']		= 401;
					$data['param']		= $picking_code;
					$data['message']	= $picking_code . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function getBatch($batch_code = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $batch_code == ''){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$results	= $this->api_picking_model->getBatch($batch_code)->result_array();
				if($results){
						$data['status']		= 200;
						$data['param']		= $batch_code;
						$data['message']	= $batch_code . ' is available.';
						$data['response']	= true;
						$data['results'] 	= $results;
				} else{
					$data['status']		= 401;
					$data['param']		= $batch_code;
					$data['message']	= $batch_code . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function setPicking()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$params['picking_code']		= $this->input->post('picking_code');
				$params['batch_code']		= $this->input->post('batch_code');
				$params['kd_barang']		= $this->input->post('item_code');
				$params['kd_unik']			= $this->input->post('serial_number');
				$params['uname_pick']		= $this->input->post('uname_pick');

				$this->api_picking_model->setPicking($params);

				$data['status']		= 200;
				$data['param']		= $params['kd_unik'];
				$data['message']	= 'Data has been updated.';
				$data['response']	= true;
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function lockPicking()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$params['picking_code']		= $this->input->post('picking_code');
				$params['remark']			= $this->input->post('remark');
				$params['picking_time']		= $this->input->post('picking_time');

				$this->api_picking_model->lockPicking($params);

				$data['status']		= 200;
				$data['param']		= $params['picking_code'];
				$data['message']	= 'Data has been updated.';
				$data['response']	= true;
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

}

/* End of file api_picking.php */
/* Location: ./application/controllers/api_picking.php */