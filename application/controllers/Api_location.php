<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_location extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('api_location_model');
		$this->load->model('api_user_model');
	}

	public function index()
	{
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;
		echo json_encode($data);
	}

	public function isValidLocation($location = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $location == ''){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$isValid = $this->api_location_model->isValidLocation($location)->row_array();
				if($isValid){
					$data['status']		= 200;
					$data['param']		= $location;
					$data['message']	= $location . ' is valid.';
					$data['response']	= true;
				} else{
					$data['status']		= 401;
					$data['param']		= $location;
					$data['message']	= $location . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

}

/* End of file api_location.php */
/* Location: ./application/controllers/api_location.php */