<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_putaway extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('api_putaway_model');
		$this->load->model('api_user_model');
	}

	public function index()
	{
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;
		echo json_encode($data);
	}

	public function getPutaway($loc = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $loc == ''){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$data['status']		= 200;
				$data['message']	= 'Request successfully.';
				$data['response']	= true;
				$data['results'] 	= $this->api_putaway_model->getPutawayList($loc)->result_array();
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function isAvailableSN($serial_number = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $serial_number == ''){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$isAvailable = $this->api_putaway_model->isAvailableSN($serial_number)->row_array();
				if($isAvailable){
					if($isAvailable['loc_id'] == 100 || $isAvailable['loc_id'] == 101 || $isAvailable['loc_id'] == 102){
						$data['status']		= 200;
						$data['param']		= $serial_number;
						$data['message']	= $serial_number . ' is available.';
						$data['response']	= true;
					} else{
						$data['status']		= 401;
						$data['param']		= $serial_number;
						$data['message']	= $serial_number . ' is not available.';
						$data['response']	= false;
					}
				} else{
					$data['status']		= 401;
					$data['param']		= $serial_number;
					$data['message']	= $serial_number . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function setPutaway()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$params['kd_unik']			= $this->input->post('serial_number');
				$params['loc_name']			= $this->input->post('loc_name');
				$params['user_name']		= $this->input->post('uname');

				$this->api_putaway_model->setPutaway($params);

				$data['status']		= 200;
				$data['param']		= $params['kd_unik'];
				$data['message']	= 'Data has been updated.';
				$data['response']	= true;
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

}

/* End of file api_putaway.php */
/* Location: ./application/controllers/api_putaway.php */