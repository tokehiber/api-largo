<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_settings extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('api_settings_model');
		$this->load->model('api_user_model');
	}

	public function index()
	{
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;
		echo json_encode($data);
	}

	public function getLocation()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$data['status']				= 200;
				$data['message']			= 'Request successfully.';
				$data['response']			= true;
				$data['resultLocation'] 	= $this->api_settings_model->getLocationList()->result_array();
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

}

/* End of file api_settings.php */
/* Location: ./application/controllers/api_settings.php */