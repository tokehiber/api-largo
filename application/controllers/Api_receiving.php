<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_receiving extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('api_receiving_model');
		$this->load->model('api_user_model');
	}

	public function index()
	{
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;
		echo json_encode($data);
	}

	public function getReceiving($receiving_code = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $receiving_code == ''){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$receivedBefore	= $this->api_receiving_model->isReceived($receiving_code)->row_array();
				if($receivedBefore){
					if($receivedBefore['st_receiving'] == 1){
						$data['status']		= 401;
						$data['param']		= $receiving_code;
						$data['message']	= $receiving_code . ' has been received before.';
						$data['response']	= false;
					} else{
						$data['status']		= 200;
						$data['param']		= $receiving_code;
						$data['message']	= $receiving_code . ' is available.';
						$data['response']	= true;
						$data['results'] 	= $this->api_receiving_model->getItemCode($receiving_code)->result_array();
					}
				} else{
					$data['status']		= 401;
					$data['param']		= $receiving_code;
					$data['message']	= $receiving_code . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function setReceiving()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$params['kd_receiving']		= $this->input->post('receiving_code');
				$params['kd_batch']			= $this->input->post('batch_code');
				$params['kd_barang']		= $this->input->post('item_code');
				$params['kd_unik']			= $this->input->post('serial_number');
				$params['tgl_exp']			= $this->input->post('tgl_exp');
				$params['tgl_in']			= $this->input->post('tgl_in');
				$params['user_name']		= $this->input->post('uname');

				$this->api_receiving_model->setReceiving($params);

				$data['status']		= 200;
				$data['param']		= $params['kd_unik'];
				$data['message']	= 'Data has been updated.';
				$data['response']	= true;
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function lockReceiving()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$params['kd_receiving']			= $this->input->post('receiving_code');
				$params['tanggal_receiving']	= $this->input->post('receiving_time');
				$params['remark']				= $this->input->post('reason');

				$this->api_receiving_model->lockReceiving($params);

				$data['status']		= 200;
				$data['param']		= $params['kd_receiving'];
				$data['message']	= 'Data has been updated.';
				$data['response']	= true;
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

}

/* End of file api_receiving.php */
/* Location: ./application/controllers/api_receiving.php */