<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_ping extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model("api_ping_model");
	}

	public function index()
	{
		echo "Not allowed";
	}

	public function doPing()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			echo json_encode($data);
		} else{
			$app_name 	= $this->input->post("app_name");
			$server 	= $this->api_ping_model->getServer($app_name)->row_array();

			if($server){
				$data['app_name'] 	= $server['app_name'];
				$data['response'] 	= true;
				echo json_encode($data);
			} else{
				$response  = array('response' => false);
				echo json_encode($response);
			}
		}
	}

}

/* End of file api_ping.php */
/* Location: ./application/controllers/api_ping.php */