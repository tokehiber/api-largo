<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_cycle_count extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('api_cycle_count_model');
		$this->load->model('api_user_model');
	}

	public function index()
	{
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;
		echo json_encode($data);
	}

	public function getCC($cc_code = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $cc_code == ''){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$cycledBefore = $this->api_cycle_count_model->isCycled($cc_code)->row_array();
				if($cycledBefore){
					if($cycledBefore['status'] == 1){
						$data['status']		= 401;
						$data['param']		= $cc_code;
						$data['message']	= $cc_code . ' has been cycled before.';
						$data['response']	= false;
					} else{
						$data['status']		= 200;
						$data['param']		= $cc_code;
						$data['message']	= $cc_code . ' is available.';
						$data['response']	= true;
						$data['results'] 	= $this->api_cycle_count_model->getItemCode($cc_code)->result_array();
					}
				} else{
					$data['status']		= 401;
					$data['param']		= $cc_code;
					$data['message']	= $cc_code . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function setCC()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$params['cc_code']		= $this->input->post('cc_code');
				$params['kd_unik']		= $this->input->post('serial_number');
				$params['uname_cc']		= $this->input->post('uname_cc');

				$this->api_cycle_count_model->setCC($params);

				$data['status']		= 200;
				$data['param']		= $params['kd_unik'];
				$data['message']	= 'Data has been updated.';
				$data['response']	= true;
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function lockCC()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$params['cc_code']	= $this->input->post('cc_code');
				$params['cc_time']	= $this->input->post('cycle_time');
				$params['remark']	= $this->input->post('reason');

				$this->api_cycle_count_model->lockCC($params);

				$data['status']		= 200;
				$data['param']		= $params['cc_code'];
				$data['message']	= 'Data has been updated.';
				$data['response']	= true;
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

}

/* End of file api_cycle_count.php */
/* Location: ./application/controllers/api_cycle_count.php */