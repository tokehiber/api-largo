<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_transfer extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('api_transfer_model');
		$this->load->model('api_user_model');
	}

	public function index()
	{
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;
		echo json_encode($data);
	}

	public function isTransfered($transfer_code = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $transfer_code == ''){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized){
				$isTransfered = $this->api_transfer_model->isTransfered($transfer_code)->row_array();
				if($isTransfered){
					if($isTransfered['transfer_status'] == 1){
						$data['status']		= 401;
						$data['param']		= $transfer_code;
						$data['message']	= $transfer_code . ' has been transfered before.';
						$data['response']	= false;
					} else{
						$data['status']		= 200;
						$data['param']		= $transfer_code;
						$data['message']	= $transfer_code . ' is available.';
						$data['response']	= true;
					}
				} else{
					$data['status']		= 401;
					$data['param']		= $transfer_code;
					$data['message']	= $transfer_code . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function lockTransfer()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$transfer_code = $this->input->post('transfer_code');
				$isAvailable = $this->api_transfer_model->isTransfered($transfer_code)->row_array();
				if($isAvailable){
					$data['status']		= 200;
					$data['param']		= $transfer_code;
					$data['message']	= $transfer_code . ' has been locked.';
					$data['response']	= true;
					$this->api_transfer_model->lockTransfer($transfer_code);
				} else{
					$data['status']		= 401;
					$data['param']		= $transfer_code;
					$data['message']	= $transfer_code . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function getTransfer()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$params['transfer_code'] 	= $this->input->post('transfer_code');
				$params['kd_unik'] 			= $this->input->post('serial_number');
				$params['uname_pick'] 		= $this->input->post('uname_pick');
				$params['state']			= 'pick';
				$isAvailable 				= $this->api_transfer_model->getTransfer($params)->row_array();
				if($isAvailable){
					$params['loc_id_old'] 	= $isAvailable['loc_id'];
					$this->api_transfer_model->setTransfer($params);
					$data['status']		= 200;
					$data['param']		= $params['kd_unik'];
					$data['location']	= $isAvailable['loc_name'];
					$data['message']	= $params['kd_unik'] . ' is ready to moved.';
					$data['response']	= true;
				} else{
					$data['status']		= 401;
					$data['param']		= $params['kd_unik'];
					$data['message']	= $params['kd_unik'] . 'is not available';
					$data['response']	= false;
				}
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function setTransfer()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$params['transfer_code'] 	= $this->input->post('transfer_code');
				$params['kd_unik'] 			= $this->input->post('serial_number');
				$params['uname_pick'] 		= $this->input->post('uname_pick');
				$params['uname_put'] 		= $this->input->post('uname_put');
				$params['loc_name_old'] 	= $this->input->post('loc_name_old');
				$params['loc_name_new']		= $this->input->post('loc_name_new');
				$params['put_time']			= $this->input->post('put_time');
				$params['state']			= 'put';
				$isAvailable 				= $this->api_transfer_model->getTransfer($params)->row_array();
				if($isAvailable){
					$this->api_transfer_model->setTransfer($params);
					$data['status']		= 200;
					$data['transfer']	= $params['transfer_code'];
					$data['param']		= $params['kd_unik'];
					$data['location']	= $params['loc_name_new'];
					$data['message']	= $params['kd_unik'] . 'hes been moved to ' . $params['loc_name_new'];
					$data['response']	= true;
				} else{
					$data['status']		= 401;
					$data['param']		= $params['kd_unik'];
					$data['message']	= $params['kd_unik'] . 'is not available';
					$data['response']	= false;
				}
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function updateByDelete()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$params['transfer_code'] 	= $this->input->post('transfer_code');
				$params['kd_unik'] 			= $this->input->post('serial_number');
				$params['loc_name']			= $this->input->post('location');
				$this->api_transfer_model->updateByDelete($params);
				$data['status']		= 200;
				$data['response']	= true;
				$data['transfer']	= $params['transfer_code'];
				$data['param']		= $params['kd_unik'];
				$data['location']	= $params['loc_name'];
				$data['message']	= $params['kd_unik'] . ' has been removed.';
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

}

/* End of file api_transfer.php */
/* Location: ./application/controllers/api_transfer.php */