<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_serial_number extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('api_serial_number_model');
		$this->load->model('api_user_model');
	}

	public function index()
	{
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;
		echo json_encode($data);
	}

	public function isValidSNReceiving($serial_number = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $serial_number == ''){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$isValid	= $this->api_serial_number_model->isValid($serial_number)->row_array();
				if($isValid){
					if($isValid['id_barang'] != ''){
						$data['status']		= 401;
						$data['param']		= $serial_number;
						$data['message']	= $serial_number . ' is not available.';
						$data['response']	= false;
					} else{
						$data['status']		= 200;
						$data['param']		= $serial_number;
						$data['message']	= $serial_number . ' is available.';
						$data['response']	= true;
					}
				} else{
					$data['status']		= 401;
					$data['param']		= $serial_number;
					$data['message']	= $serial_number . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function isValidSNPicking($item_code = '', $serial_number = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $item_code == '' || $serial_number == ''){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$isValid	= $this->api_serial_number_model->isValidPicking($item_code,$serial_number)->row_array();
				if($isValid){
					if($isValid['pl_id'] != '' || $isValid['pl_status'] != 0){
						$data['status']		= 401;
						$data['param']		= $serial_number;
						$data['message']	= $serial_number . ' is not available.';
						$data['response']	= false;
					} else{
						$data['status']		= 200;
						$data['param']		= $serial_number;
						$data['message']	= $serial_number . ' is available.';
						$data['response']	= true;
					}
				} else{
					$data['status']		= 401;
					$data['param']		= $serial_number;
					$data['message']	= $serial_number . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function activeStock($serial_number = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $serial_number == ''){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$isValid	= $this->api_serial_number_model->activeStock($serial_number)->row_array();
				if($isValid){
					$data['status']		= 200;
					$data['param']		= $serial_number;
					$data['message']	= $serial_number . ' is available.';
					$data['response']	= true;
					$data['item_code'] 	= $isValid['kd_barang'];
					$data['item_name'] 	= $isValid['nama_barang'];
					$data['location'] 	= $isValid['loc_name'];
					$data['date_in'] 	= $isValid['tgl_in'];
					$data['exp_date'] 	= $isValid['tgl_exp'];
				} else{
					$data['status']		= 401;
					$data['param']		= $serial_number;
					$data['message']	= $serial_number . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function activeDetail($item_code = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $item_code == ''){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$isValid	= $this->api_serial_number_model->isValidActiveDetail($item_code)->row_array();
				if($isValid){
					$activeQTY			= $this->api_serial_number_model->countActiveStock($item_code)->row_array();
					$data['status']		= 200;
					$data['param']		= $item_code;
					$data['message']	= $item_code . ' is available.';
					$data['response']	= true;
					$data['item_name']	= $isValid['nama_barang'];
					$data['qty']		= $activeQTY['qty'];
					$data['results']	= $this->api_serial_number_model->getActiveDetail($item_code)->result_array();
				} else{
					$data['status']		= 401;
					$data['param']		= $item_code;
					$data['message']	= $item_code . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

}

/* End of file api_serial_number.php */
/* Location: ./application/controllers/api_serial_number.php */