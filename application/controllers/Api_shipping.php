<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_shipping extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('api_shipping_model');
		$this->load->model('api_user_model');
	}

	public function index()
	{
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;
		echo json_encode($data);
	}

	public function getShipping($shipping_code = '')
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $shipping_code == ''){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$shippedBefore	= $this->api_shipping_model->isShipped($shipping_code)->row_array();
				if($shippedBefore){
					if($shippedBefore['st_shipping'] == 1){
						$data['status']		= 401;
						$data['param']		= $shipping_code;
						$data['message']	= $shipping_code . ' has been shipped before.';
						$data['response']	= false;
					} else{
						$data['status']		= 200;
						$data['param']		= $shipping_code;
						$data['message']	= $shipping_code . ' is available.';
						$data['response']	= true;
						$data['results'] 	= $this->api_shipping_model->getItemCode($shipping_code)->result_array();
					}
				} else{
					$data['status']		= 401;
					$data['param']		= $shipping_code;
					$data['message']	= $shipping_code . ' is not valid.';
					$data['response']	= false;
				}
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function setShipping()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$params['shipping_code']	= $this->input->post('shipping_code');
				$params['kd_barang']		= $this->input->post('item_code');
				$params['kd_unik']			= $this->input->post('serial_number');
				$params['uname_ship']		= $this->input->post('uname_ship');

				$this->api_shipping_model->setShipping($params);

				$data['status']		= 200;
				$data['param']		= $params['kd_unik'];
				$data['message']	= 'Data has been updated.';
				$data['response']	= true;
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

	public function lockShipping()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			$data['response']	= false;
		} else{
			$isAuthorized = $this->api_user_model->isAuthorized();
			if($isAuthorized == true){
				$params['shipping_code']	= $this->input->post('shipping_code');
				$params['shipping_date']	= $this->input->post('shipping_date');

				$this->api_shipping_model->lockShipping($params);

				$data['status']		= 200;
				$data['param']		= $params['shipping_code'];
				$data['message']	= 'Data has been updated.';
				$data['response']	= true;
			} else{
				$data['status']		= 401;
				$data['message']	= 'Unauthorized user.';
				$data['response']	= false;
			}
		}
		echo json_encode($data);
	}

}

/* End of file api_shipping.php */
/* Location: ./application/controllers/api_shipping.php */