<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_user extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model("api_user_model");
		$this->load->library("randomidgenerator");
	}

	public function index()
	{
		$data['status']		= 400;
		$data['message']	= 'Bad request.';
		$data['response']	= false;
		echo json_encode($data);
	}

	public function doLogin()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			echo json_encode($data);
		} else{
			$uname 	= $this->input->post("uname");
			$upass 	= md5($this->input->post("upass"));
			$user 	= $this->api_user_model->getUser($uname, $upass)->row_array();

			if($user){
				$data['uid'] 					= $user['user_id'];
				$data['uname']					= $user['user_name'];
				$data['handheld_session_code'] 	= $this->randomidgenerator->createSessionToken();
				$data['response'] 				= true;
				$this->api_user_model->updateUserSessionCode($data['uid'], $data['handheld_session_code']);
				echo json_encode($data);
			} else{
				$response = array('response' => false);
				echo json_encode($response);
			}
		}
	}

	public function isUserValid()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$data['status']		= 400;
			$data['message']	= 'Bad request.';
			echo json_encode($data);
		} else{
			$uname			= $this->input->post("uname");
			$session_code	= $this->input->post("session_code");
			$valid 			= $this->api_user_model->getUserValidation($uname, $session_code)->row_array();

			if($valid){
				$data['response'] = true;
				echo json_encode($data);
			} else{
				$response = array('response' => false);
				echo json_encode($response);
			}
		}
	}

}

/* End of file api_user.php */
/* Location: ./application/controllers/api_user.php */